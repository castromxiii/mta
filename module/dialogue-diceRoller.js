export class DiceRollerDialogue extends Application {
  constructor({dicePool=0, targetNumber=8, flavor="Skill Check", addBonusFlavor=false, title="Skill Check", blindGMRoll=false, actorOverride=undefined}, ...args){
    super(...args);
    this.targetNumber = +targetNumber;
    this.dicePool = +dicePool;
    this.flavor = flavor;
    this.addBonusFlavor = addBonusFlavor;
    this.blindGMRoll = blindGMRoll;
    this.options.title = title;
    this.actorOverride = actorOverride;
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "dialogue", "mta-sheet"],
  	  template: "systems/mta/templates/dialogues/dialogue-diceRoller.html",
      resizable: true
    });
  }
  
  getData() {
    const data = super.getData();
    data.targetNumber = this.targetNumber;
    data.dicePool = this.dicePool;
    data.bonusDice = 0;
    
    if(game.settings.get("mta", "showRollDifficulty")) data.enableDifficulty = true;

    return data;
  }
  
  _fetchInputs(html){
    const dicePool_userMod_input = html.find('[name="dicePoolBonus"]');
    const dicePool_difficulty_input = html.find('[name="dicePoolDifficulty"]');
    
    let dicePool_userMod = dicePool_userMod_input.length ? +dicePool_userMod_input[0].value : 0;
    let explode_threshold = Math.max(0,+($('input[name=explodeThreshold]:checked').val()));
    let rote_action = $('input[name=rote_action]').prop("checked");

    let dicePool_difficulty 
    if(game.settings.get("mta", "showRollDifficulty")) dicePool_difficulty = dicePool_difficulty_input.length ? +dicePool_difficulty_input[0].value : 0;
    else dicePool_difficulty = 8;
    
    return {dicePool_userMod: dicePool_userMod, explode_threshold: explode_threshold, rote_action: rote_action, dicePool_difficulty: dicePool_difficulty}
  }
  
  activateListeners(html) {
    super.activateListeners(html);
    
    html.find('.roll-execute').click(ev => {  
      const modifiers = this._fetchInputs(html);
      let dicePool = this.dicePool + modifiers.dicePool_userMod;
      let roteAction = modifiers.rote_action;
      let flavor = this.flavor ? this.flavor : "Skill Check";
      flavor += modifiers.dicePool_userMod>0 ? " + " + modifiers.dicePool_userMod : modifiers.dicePool_userMod<0 ? " - " + -modifiers.dicePool_userMod : "";
      let explodeThreshold = modifiers.explode_threshold;
      let targetNumber = clampNumber(modifiers.dicePool_difficulty, 1, 10);
      
      DiceRollerDialogue.rollToChat({dicePool: dicePool, targetNumber: targetNumber, tenAgain: explodeThreshold===10, nineAgain: explodeThreshold===9, eightAgain: explodeThreshold===8, roteAction: roteAction, flavor: flavor, blindGMRoll: this.blindGMRoll, actorOverride: this.actorOverride});
    });
  }
  
  
  static _roll({dicePool=1, targetNumber=8, tenAgain=true, nineAgain=false, eightAgain=false, roteAction=false, chanceDie=false, exceptionalTarget=5}){
    //Create dice pool qualities
    const roteActionString = roteAction ? "r<8" : "";
    const explodeString = eightAgain ? "x>=8" : nineAgain ? "x>=9" : tenAgain ? "x>=10" : "" ;
    const targetNumString = chanceDie ? "cs>=10" : "cs>=" + targetNumber;
    
    let roll = new Roll("(@dice)d10" + roteActionString + explodeString + targetNumString, {
      dice: dicePool, targetNumber
    }).roll();
    
    if(chanceDie && roteAction && roll.terms[0].results[0].result === 1){
      //Chance dice don't reroll 1s with Rote quality
      roll.terms[0].results.splice(1);
    }
    if(chanceDie && roll.terms[0].results[0].result === 1) roll.dramaticFailure = true;
    if(roll.total >= exceptionalTarget) roll.exceptionalSuccess = true;

    return roll;
  }
  
  
  static async rollToHtml({dicePool=1, targetNumber=8, tenAgain=true, nineAgain=false, eightAgain=false, roteAction=false, flavor="Skill Check", showFlavor=true, exceptionalTarget=5}){   
    //Is the roll a chance die?
    let chanceDie = false;
    if(dicePool < 1) {
      tenAgain = false;
      chanceDie = true;
      dicePool = 1;
    }
    
    let roll = DiceRollerDialogue._roll({dicePool: dicePool, targetNumber: targetNumber, tenAgain: tenAgain, nineAgain: nineAgain, eightAgain: eightAgain, roteAction: roteAction, chanceDie: chanceDie, exceptionalTarget: exceptionalTarget});
    
    //Create Roll Message
    let speaker = ChatMessage.getSpeaker();
    
    if(chanceDie) flavor += " [Chance die]";
    if(roteAction) flavor += " [Rote quality]";
    if(eightAgain) flavor += " [8-again]";
    else if(nineAgain) flavor += " [9-again]";
    else if(tenAgain) flavor += " [10-again]";
    if(!showFlavor) flavor = undefined;

    let html = await roll.render({speaker: speaker, flavor: flavor});
    if(roll.dramaticFailure) html = html.replace('class="dice-total"', 'class="dice-total dramaticFailure"');
    else if(roll.exceptionalSuccess) html = html.replace('class="dice-total"', 'class="dice-total exceptionalSuccess"');

    return html;
  }

  
  static async rollToChat({dicePool=1, targetNumber=8, tenAgain=true, nineAgain=false, eightAgain=false, roteAction=false, exceptionalTarget=5, flavor="Skill Check", blindGMRoll=false, actorOverride=undefined}){

    const templateData = {
      roll: await DiceRollerDialogue.rollToHtml({dicePool: dicePool, targetNumber: targetNumber, tenAgain: tenAgain, nineAgain: nineAgain, eightAgain: eightAgain, roteAction: roteAction, exceptionalTarget: exceptionalTarget, showFlavor: false})
    };

    //Create Roll Message
    let rollMode = blindGMRoll ? "blindroll" : game.settings.get("core", "rollMode");
    let speaker = actorOverride ? ChatMessage.getSpeaker({actor: actorOverride}) : ChatMessage.getSpeaker();

    // Render the chat card template
    const template = `systems/mta/templates/chat/roll-template.html`;
    const html = await renderTemplate(template, templateData);

    // Basic chat message data
    const chatData = {
      user: game.user._id,
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      content: html,
      speaker: speaker,
      flavor: flavor,
      sound: CONFIG.sounds.dice
    };

    // Toggle default roll mode
    if ( ["gmroll", "blindroll"].includes(rollMode) ) chatData["whisper"] = ChatMessage.getWhisperRecipients("GM");
    if ( rollMode === "blindroll" ) chatData["blind"] = true;

    // Create the chat message
    return ChatMessage.create(chatData);
  }
  
  //Deprecated
  static createDiceRollMessage({dicePool=1, targetNumber=8, explodeThreshold=10, roteAction=false, blindGMRoll=false, chanceDie=false}={}, flavor="Skill Check", addBonusFlavor, bonus){
      if(+targetNumber > 0){
        //Roll
        if(+dicePool < 1){
          dicePool = 1;
          chanceDie = true;
        }
        if(chanceDie) targetNumber = 10;
        
        let roteActionString = roteAction ? "r<8" : "";
        if(chanceDie && explodeThreshold===10) explodeThreshold = 11;
        let explodeString = explodeThreshold<=10 ? "x>=" + explodeThreshold : "";
        
        let roll = new Roll("(@dice)d10" + roteActionString + explodeString + "cs>=(@diff)", {
          dice: dicePool,
          diff: targetNumber
        }).roll();
        
        if(chanceDie && roteAction && roll._dice[0].rolls[0].roll === 1){
          roll._dice[0].rolls.splice(1);
        }
        
        //Create Roll Message
        let rollMode = blindGMRoll ? "blindroll" : game.settings.get("core", "rollMode");
        let speaker = ChatMessage.getSpeaker();
        
        if(addBonusFlavor) flavor += " + " + bonus;
        if(chanceDie) flavor += " [Chance die]";
        if(roteAction) flavor += " [Rote quality]";
        if(explodeThreshold === 8) flavor += " [8-again]";
        else if(explodeThreshold === 9) flavor += " [9-again]";
        else if(explodeThreshold === 10) flavor += " [10-again]";
        
        // Convert the roll to a chat message and return the roll
        return roll.toMessage({
          speaker: speaker,
          flavor: flavor
        }, { rollMode });
      }
    else console.log("ERROR: target number is 0 or lower!");
  }

}