// Import Modules
import {
  MtAActorSheet
} from "./actor-sheet.js";
import {
  MtAItemSheet
} from "./item-sheet.js";
import {
  ItemMtA
} from "./item.js";
import {
  ActorMtA
} from "./actor.js";
import {
  DiceRollerDialogue
} from "./dialogue-diceRoller.js"
import { registerSystemSettings } from "./settings.js";
import * as templates from "./templates.js";
import * as migrations from "./migration.js";
import { MTA } from "./config.js";
import {
  TokenHotBar
} from "./token-macrobar.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function () {
  console.log(`Initializing MtA System`);
  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.MTA = MTA;

  CONFIG.Combat.initiative.formula = "1d10 + @derivedTraits.initiativeMod.computed";

  CONFIG.Item.entityClass = ItemMtA;
  CONFIG.Actor.entityClass = ActorMtA;
  
  CONFIG.JournalEntry.noteIcons = { //Removed: Barrel, Castle, Coins, Fire, Hanging Sign, Pawprint, Statue, Sword, Temple, Waterfall, Windmill
    Airport: "systems/mta/icons/notes/airport.svg",
    Anchor: "icons/svg/anchor.svg",
    Bag: "systems/mta/icons/notes/bag.svg",
    Barracks: "systems/mta/icons/notes/barracks.svg",
    Book: "icons/svg/book.svg",
    Bridge: "systems/mta/icons/notes/bridge.svg",
    Cave: "icons/svg/cave.svg",
    Chest: "icons/svg/chest.svg",
    City: "systems/mta/icons/notes/city.svg",
    Computer: "systems/mta/icons/notes/computer.svg",
    House: "icons/svg/house.svg",
    Island: "systems/mta/icons/notes/island.svg",
    Key: "systems/mta/icons/notes/key.svg",
    Mountain: "icons/svg/mountain.svg",
    'Oak Tree': "icons/svg/oak.svg",
    Obelisk: "icons/svg/obelisk.svg",
    Pentagram: "systems/mta/icons/notes/pentagram.svg",
    Pin: "systems/mta/icons/notes/pin.svg",
    Portal: "systems/mta/icons/notes/portal.svg",
    Ruins: "icons/svg/ruins.svg",
    Shop: "systems/mta/icons/notes/shop.svg",
    Skull: "icons/svg/skull.svg",
    Spell: "systems/mta/icons/notes/spell.svg",
    Spirit: "systems/mta/icons/notes/spirit.svg",
    Tankard: "icons/svg/tankard.svg",
    'Temple Gate': "systems/mta/icons/notes/temple-gate.svg",
    Totem: "systems/mta/icons/notes/totem.svg",
    Tower: "systems/mta/icons/notes/tower.svg",
    Trap: "icons/svg/trap.svg",
    Village: "icons/svg/village.svg"
  } 
  //Updated: Bridge, City, Tower
  //Added: Island, Shop, Pin, Magic Portal, Spell, Computer, Bag, Pentagram, Temple Gate, Totem, Key, Airport, Barracks, Spirit
  
  // Register System Settings
  registerSystemSettings();

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("dnd5e", MtAActorSheet, {
    makeDefault: true
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("dnd5e", MtAItemSheet, {
    makeDefault: true
  });
  // Preload Handlebars Templates
  templates.preloadHandlebarsTemplates();
  templates.registerHandlebarsHelpers();
});

/**
 * This function runs after game data has been requested and loaded from the servers, so entities exist
 */
Hooks.once("setup", function() {

  // Localize CONFIG objects once up-front
  const toLocalize = [
    "eph_physical",
    "eph_social",
    "eph_mental",
    "attributes_physical",
    "attributes_social",
    "attributes_mental",
    "skills_physical",
    "skills_social",
    "skills_mental",
    "derivedTraits",
    "arcana",
    "arcana_gross",
    "arcana_subtle",
    "disciplines_common",
    "disciplines_unique",
    "vampire_traits",
    "changeling_traits",
    "werewolf_renown",
    "werewolf_traits",
    "giftTypes"
  ];

  // Exclude some from sorting where the default order matters
  const noSort = [
    "eph_physical",
    "eph_social",
    "eph_mental",
    "attributes_physical",
    "attributes_social",
    "attributes_mental",
    "derivedTraits",
    "giftTypes"
  ];

  // Localize and sort CONFIG objects
  for ( let o of toLocalize ) {
    const localized = Object.entries(CONFIG.MTA[o]).map(e => {
      return [e[0], game.i18n.localize(e[1])];
    });
    if ( !noSort.includes(o) ) localized.sort((a, b) => a[1].localeCompare(b[1]));
    CONFIG.MTA[o] = localized.reduce((obj, e) => {
      obj[e[0]] = e[1];
      return obj;
    }, {});
  }
  
});

/**
 * Once the entire VTT framework is initialized, check to see if we should perform a data migration.
 * Small version changes (after the last dot) do not need a migration.
 */
Hooks.once("ready", function() {
  // Determine whether a system migration is required and feasible
  const currentVersion = game.settings.get("mta", "systemMigrationVersion");
  const migrationVersion = game.data.system.data.version;
  console.log("MTA Current Version: " + currentVersion);
  let version_nums_current = currentVersion.split(".").map(x=>+x);
  let version_nums_migration = migrationVersion.split(".").map(x=>+x);
  //const NEEDS_MIGRATION_VERSION = 0.84;
  //const COMPATIBLE_MIGRATION_VERSION = 0.80;
  let needMigration = (version_nums_current[0] < version_nums_migration[0]) || (version_nums_current[0] === version_nums_migration[0] && version_nums_current[1] < version_nums_migration[1]) || (currentVersion === null);

  // Perform the migration
  if(game.user.isGM){
    if ( needMigration) {
      migrations.migrateWorld(version_nums_current, version_nums_migration);
    }
    else if(!migrations.compareVersion(version_nums_current, version_nums_migration) && (version_nums_current[0] !== version_nums_migration[0] || version_nums_current[1] !== version_nums_migration[1] || version_nums_current[2] !== version_nums_migration[2])){
      game.settings.set("mta", "systemMigrationVersion", game.data.system.data.version);
      ui.notifications.info(`MtA System downgraded to version ${game.system.data.version}.`, {permanent: true});
    }
  }
  CONFIG.MTA.TOKENBAR = TokenHotBar.tokenHotbarInit();
  debounce(createTokenBar, 200);
});



Hooks.on("renderChatMessage", (message, html, data) => {
  
  // Optionally collapse the description
  if (game.settings.get("mta", "autoCollapseItemDescription")) html.find(".card-description").hide();
  
  const chatCard = html.find(".chat-card");
  if (chatCard.length === 0) {
    return;
  }
  
  // If the user is the message author or the actor owner, proceed
  let actor = game.actors.get(data.message.speaker.actor);
  if (actor && actor.owner) return;
  else if (game.user.isGM || (data.author.id === game.user.id)) return;

  // Otherwise hide action buttons
  const buttons = chatCard.find("button[data-action]");
  buttons.each((i, btn) => {
    btn.style.display = "none"
  });
});

Hooks.on("renderChatLog", (app, html, data) => ItemMtA.chatListeners(html));

//Dice Roller
$(document).ready(() => {
  const diceIconSelector = '#chat-controls .chat-control-icon .fa-dice-d20';

  $(document).on('click', diceIconSelector, ev => {
    ev.preventDefault();
    let diceRoller = new DiceRollerDialogue({});
    diceRoller.render(true);
  });
});

Hooks.on("renderActorDirectory", (app, html, data) => {
  const actorListItems = html.find('li');
  
  actorListItems.toArray().forEach(v => {
    if(v.dataset.entityId){
      const actor = game.actors.get(v.dataset.entityId);
      if(actor){
        //Adds colored border to characters based on their type
        const characterType = actor.data.type === "character" ? actor.data.data.characterType : actor.data.data.ephemeralType;
        const color = game.user.isGM || actor.data.data.isTypeKnown ? CONFIG.MTA.typeColors[characterType] : CONFIG.MTA.typeColors["unknown"];
        $(v).find('img').css("border", "1px solid " + color);
        //$(v).find('a').css("color", color);

      }
      else console.log("ERROR: invalid actor found.")
    }
  });
});


let createTokenBar = (token, select) =>
  {
    let controlled = canvas.tokens.controlled;
    if(controlled.length){
      CONFIG.MTA.TOKENBAR.tokens = controlled;
      CONFIG.MTA.TOKENBAR.render(true);
    }
    else{
      CONFIG.MTA.TOKENBAR.close();
    }  
  }

Hooks.on("controlToken", debounce(createTokenBar, 200));

Hooks.on("updateActor", debounce(createTokenBar, 200));
Hooks.on("updateOwnedItem", debounce(createTokenBar, 200));
Hooks.on("updateToken", debounce(createTokenBar, 200));